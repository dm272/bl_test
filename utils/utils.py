import sys

from word2number import w2n


class Utils:
    @staticmethod
    def filter_compare(arr, search_key, index):
        return list(filter(lambda obj: obj[index] == search_key, arr))

    @staticmethod
    def filter_contains(arr, search_key, index):
        return list(filter(lambda obj: search_key in obj[index], arr))

    @staticmethod
    def word2num(string):
        try:
            num = w2n.word_to_num(string)
            return num
        except Exception as e:
            for s in string.split():
                if s.isdigit():
                    return int(s)
            return None

    # Based on: https://goshippo.com/blog/measure-real-size-any-python-object/
    @staticmethod
    def get_size(obj, seen=None):
        """Recursively finds size of objects"""
        size = sys.getsizeof(obj)
        if seen is None:
            seen = set()
        obj_id = id(obj)
        if obj_id in seen:
            return 0
        # Important mark as seen *before* entering recursion to gracefully handle
        # self-referential objects
        seen.add(obj_id)
        if isinstance(obj, dict):
            size += sum([Utils.get_size(v, seen) for v in obj.values()])
            size += sum([Utils.get_size(k, seen) for k in obj.keys()])
        elif hasattr(obj, '__dict__'):
            size += Utils.get_size(obj.__dict__, seen)
        elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
            size += sum([Utils.get_size(i, seen) for i in obj])
        return size
