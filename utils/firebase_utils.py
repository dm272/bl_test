import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

print('Initializing firebase...')
cred = credentials.Certificate('firebase_service_account.json')
firebase_admin.initialize_app(cred)
print('Firebase initialized.')


class FirebaseUtils:

    @staticmethod
    def firestore():
        return firestore.client()
