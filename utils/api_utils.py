import requests, json


class ApiRequest:
    def __init__(self, request_method, endpoint, request_body=None):
        self.method = request_method
        self.url = endpoint
        self.headers = {
            'Content-Type': 'application/json'
        }
        self.body = json.dumps(request_body)

    def send_request(self):
        options = {
            'method': self.method,
            'url': self.url,
            'headers': self.headers,
            'body': (self.body if self.body is not None else None),
            'json': True
        }

        response = requests.request(self.method, self.url, data=self.body, headers=self.headers)
        if response.status_code != 200:
            raise Exception(f'{response.status_code} - {response.reason}')

        return response.json()


class ApiUtils:
    @staticmethod
    def generate_request(request_method, endpoint, request_body=None):
        api_request = ApiRequest(request_method, endpoint, request_body)
        return api_request
