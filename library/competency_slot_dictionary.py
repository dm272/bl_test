from .competency_slot import CompetencySlot
from typing import Dict, List


class CompetencySlotDictionary(Dict[str, CompetencySlot]):

    def __init__(self, *args, **kwargs):
        super(CompetencySlotDictionary, self).__init__(*args, **kwargs)

    def delete(self, key: str) -> CompetencySlot:
        k = self.normalize_key(key)
        return super(CompetencySlotDictionary, self).pop(k)

    def get(self, key: str) -> CompetencySlot:
        k = self.normalize_key(key)
        v = super(CompetencySlotDictionary, self).get(k)

        if v and not v.name:
            v.name = k

        return v

    def has(self, key: str) -> bool:
        k = self.normalize_key(key)
        return k in self

    def set(self, key: str, value: CompetencySlot) -> CompetencySlot:
        k = CompetencySlotDictionary.normalize_key(key)
        value.name = k
        self[k] = value

        return self[k]

    def get_slot_value(self, slot_name: str, value_index=0):
        slot = self.get(slot_name)
        value = None
        if slot:
            value = slot.get_value(value_index)

        return value

    def get_slot_values(self, slot_name: str):
        slot = self.get(slot_name)
        values = []
        if slot:
            for value in slot.values:
                values.append(value.get_value_or_tokens())
        return values

    def get_unresolved_slot_values(self, slot_name: str):
        slot = self.get(slot_name)
        values = []
        if slot:
            for value in slot.values:
                if value.resolved == -1:
                    values.append(value.get_value_or_tokens())
        return values

    def retain_slot_values(self, slots_names=None):
        if slots_names is None:
            for slot in self.values():
                slot.retain_values()
        else:
            for slot_name in slots_names:
                self.get(slot_name).retain_values()

    @staticmethod
    def normalize_key(key: str) -> str:
        k = key.upper()
        if not k.startswith('_'):
            k = '_' + k

        if not k.endswith('_'):
            k = k + '_'

        return k

    @staticmethod
    def slot_mapping_suffixes():
        return ['_dest', '_exact', '_fuzzy', '_token']

    @staticmethod
    def from_dict(params: dict):
        dictionary = CompetencySlotDictionary()

        # convert slot mapped values to slots
        if params:
            new_slots = {}
            for slot in params.values():
                for slot_value in slot['values']:
                    for slot_value_key in slot_value:
                        if any(slot_value_key.endswith(suffix) for suffix in
                               CompetencySlotDictionary.slot_mapping_suffixes()) \
                                and slot_value[slot_value_key] is not None \
                                and slot_value[slot_value_key] != 'NULL':
                            new_slot_key = f'_{slot_value_key.upper()}_'
                            if new_slot_key in new_slots:
                                new_slots[new_slot_key]['values'].append({
                                    'resolved': -1,
                                    'tokens': slot_value[slot_value_key]
                                })
                            else:
                                new_slots[new_slot_key] = {
                                    'name': new_slot_key,
                                    'type': 'string',
                                    'values': [
                                        {
                                            'resolved': -1,
                                            'tokens': slot_value[slot_value_key]
                                        }
                                    ]
                                }

            params.update(new_slots)

            for slot_key in params:
                dictionary.set(slot_key, CompetencySlot.from_dict(params[slot_key]))

        return dictionary
