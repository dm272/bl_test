import json


class CompetencyResponseSlots:
    def __init__(self):
        self.response_type: str = None
        self.visuals = {}
        self.speakables = {}

    def set_slot(self, slot_name, visual, speakable=None):
        self.visuals[slot_name] = visual
        self.speakables[slot_name] = speakable if speakable else visual

    def unset_slot(self, slot_name):
        self.visuals.pop(slot_name, None)
        self.speakables.pop(slot_name, None)

    @classmethod
    def from_dict(cls, params: dict):
        cls_instance = cls()
        for key, val in params.items():
            setattr(cls_instance, key, val)

        return cls_instance
