from abc import ABC, abstractmethod, abstractproperty

from library.competency_slot import CompetencySlot
from .competency_response import CompetencyResponse


class Competency(ABC):
    def __init__(self, params):
        self.response = CompetencyResponse.from_dict(params)

    def run(self):
        self.process()
        return self.response

    def add_intent_slot(self):
        self.response.slots.set('intent', CompetencySlot.of(self.response.intent))

    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def process(self):
        pass
