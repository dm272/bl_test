from library.competency_response_slot import CompetencyResponseSlots
from .competency_slot_dictionary import CompetencySlotDictionary
import json


class CompetencyResponse:
    def __init__(self):
        self.lat: float = None
        self.lon: float = None
        self.qid: str = None
        self.state: str = None
        self.intent: str = None
        self.dialog: str = None
        self.device: str = None
        self.query: str = None
        self.time_offset: int = None
        self.slots: CompetencySlotDictionary = CompetencySlotDictionary()
        self.response_slots: CompetencyResponseSlots = CompetencyResponseSlots()

    @staticmethod
    def from_dict(params):
        response = CompetencyResponse()
        for key, val in params.items():
            if key == 'slots':
                response.slots = CompetencySlotDictionary.from_dict(val)
            elif key == 'response_slots':
                response.response_slots = CompetencyResponseSlots.from_dict(val)
                response.response_slots.response_type = response.state
            else:
                setattr(response, key, val)

        return response

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=2)
