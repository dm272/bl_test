class CompetencySlotValue:

    def __init__(self):
        self.tokens: str = None
        self.value = None
        self.resolved: int = 1
        self.meta: dict = {}

    def get_value_or_tokens(self):
        return self.tokens if self.value is None else self.value

    @staticmethod
    def of(value):
        slot_value = CompetencySlotValue()
        slot_value.value = value
        slot_value.tokens = value
        return slot_value

    @staticmethod
    def from_dict(params):
        value = CompetencySlotValue()
        value.resolved = params['resolved']

        if 'tokens' in params:
            value.tokens = params['tokens']

        if 'value' in params:
            value.value = params['value']

        return value
