from .competency_slot_value import CompetencySlotValue
from typing import List


class CompetencySlot:
    def __init__(self):
        self.name: str = None
        self.type: str = None
        self.values: List[CompetencySlotValue] = []

    def get_value(self, value_index=0):
        value = None
        if value_index < len(self.values):
            value = self.values[value_index].get_value_or_tokens()

        return value

    def get_values(self) -> []:
        values = []

        for value in self.values:
            values.append(value.get_value_or_tokens)

        return values

    def retain_values(self):
        for value in self.values:
            value.resolved = 1
            if not (hasattr(value, 'value') and value.value):
                value.value = value.tokens

    def infer_type(self):
        if len(self.values) > 0:
            value_type = type(self.get_value())
            if value_type is int or value_type is float:
                self.type = 'number'
            elif value_type is bool:
                self.type = 'bool'
            elif value_type is str:
                self.type = 'string'
            elif value_type is dict:
                self.type = 'dict'
            elif value_type is list or value_type is tuple:
                self.type = 'list'

    def format(self):
        self.infer_type()
        return self

    @staticmethod
    def of(value):
        slot = CompetencySlot()
        slot.values.append(CompetencySlotValue.of(value))
        slot.infer_type()
        return slot

    @staticmethod
    def from_dict(params):
        slot = CompetencySlot()
        slot.type = params['type']
        for key in params['values']:
            slot.values.append(CompetencySlotValue.from_dict(key))
        return slot
