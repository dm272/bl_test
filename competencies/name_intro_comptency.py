from library.competency import Competency
from library.competency_slot import CompetencySlot
from utils.firebase_utils import FirebaseUtils


class NameIntroCompetency(Competency):

    def __init__(self, params):
        super(NameIntroCompetency, self).__init__(params)

    def get_name(self):
        return 'name_intro'

    def process(self):
        config = FirebaseUtils.firestore().collection('bl-template-py').document('config').get().to_dict() or {}

        self.response.slots.retain_slot_values()

        if self.response.slots.get('first_name'):
            config['first_name'] = self.response.slots.get_slot_value('first_name')
            FirebaseUtils.firestore().collection('bl-template-py').document('config').set(config)

            self.response.slots.set('first_name', CompetencySlot.of(config.get('first_name', '[no name set]')))
            self.response.slots.set('name_set', CompetencySlot.of(True))
        else:
            self.response.slots.set('first_name', CompetencySlot.of(config.get('first_name', '[no name set]')))
            self.response.slots.set('name_set', CompetencySlot.of(False))
