all:
	virtualenv --python=python3 venv
	chmod +x ./venv/bin/activate
	./venv/bin/activate
	pip install -r requirements.txt

config:
	echo "API_URL = '$(url)'" > config.py

start:
	python app.py
