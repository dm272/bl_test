# Business Logic Configuration

1. Run `make all` command to install all app dependencies & create/launch python virtual environment.

2. Create config.py file. Run the command `make confg url=[insert_bl_api_url]` inserting the url of the BL server you are using.

3. Run `make start` command to launch app locally for testing/debugging purposes.

# Heroku Python Deployment

1. If you haven't already download and install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-command-line).

2. Edit the Heroku Procfile and insert the url of the api you are deploying to in release phase.

3. After changes have been approved and merged to heroku branch, run `git push heroku master` to deploy.

