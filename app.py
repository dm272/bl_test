import logging

from flask import Flask
from flask import request, make_response
from library.competency_response import CompetencyResponse
from competencies.name_intro_comptency import NameIntroCompetency
import json

from library.competency_slot import CompetencySlot

app = Flask(__name__)

# Last params & response
last_params = {}
last_response = {}


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/app', methods=['POST'])
def app_run():
    global last_params, last_response
    params = request.json
    competency = None
    
    # Save last paramaters
    last_params = params
    
    # Switch competency class based on state
    if params['state'] == 'name_intro':
        competency = NameIntroCompetency(params)

    # Run selected competency
    try:
        if competency:
            data = competency.run()
        # No competency found; return empty response structure
        else:
            data = CompetencyResponse.from_dict(params)
    except Exception as e:
        logging.exception('Error running competency.')
        if type(e.args) is str:
            error_message = e.args
        else:
            error_message = json.dumps(e.args)

        print('Something went wrong. Setting error_message slot', error_message)
        data = CompetencyResponse.from_dict(params)
        data.slots.set('error_message', CompetencySlot.of(error_message))

    response_json = data.to_json()
    last_response = json.loads(response_json)
    response = make_response(response_json)
    response.mimetype = 'application/json'

    return response


@app.route('/last-params')
def show_last_params():
    global last_params
    response = make_response(json.dumps(last_params, sort_keys=True, indent=2))
    response.mimetype = 'application/json'
    return response


@app.route('/last-response')
def show_last_response():
    global last_response
    response = make_response(json.dumps(last_response, sort_keys=True, indent=2))
    response.mimetype = 'application/json'
    return response


if __name__ == '__main__':
    print('Starting app...')
    app.run()
